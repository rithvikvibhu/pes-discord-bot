const config = require('config');
const axios = require('axios');
const querystring = require('querystring');
const Discord = require('discord.js');
const admin = require("firebase-admin");
const express = require('express');


// initialize everything

// Web
const app = express();
app.get('/', (req, res) => res.send('Hello World!'))
app.get('/api/listen/:value', (req, res) => {
  if (req.params.value != 'true' && req.params.value != 'false') return res.send('No.');
  listen = (req.params.value == 'true');
  console.log('Setting listen to', listen);
  res.send('Ok.')
});
app.get('/api/sendMessage/:to/:message', (req, res) => {
  if (!req.params.to || !req.params.message) return res.send('No.');
  client.channels.get(req.params.to).send(req.params.message);
  console.log('Sent by API:', req.params.to, req.params.message);
  res.send('Ok.')
});
app.listen(process.env.PORT || 80, () => console.log(`Started web server on port ${process.env.PORT || 80}`))

// Discord
const client = new Discord.Client();
client.commands = new Discord.Collection();
var commands = require('./commands.js');
for (command of commands) {
  console.log('Registering command:', command.name);
  client.commands.set(command.name, command);
}

// Firebase
if (process.env.serviceAccountKey) {
  var serviceAccount = JSON.parse(process.env.serviceAccountKey);
} else {
  var serviceAccount = require("./config/serviceAccountKey.json");
}
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://pes-discord-bot.firebaseio.com"
});
db = admin.database();
db.ref('calendar').once('value', snaps => {
  snaps.forEach(snap => {
    calData.push(snap.val())
  });
  console.log('Imported calendar:', calData.length);
});

var firstRun = true;
var listen = true;
var counts = {};
calData = [];

client.on('ready', () => {
  console.log('Connected to Discord.');
  if (firstRun) {
    getLatestCounts().then(function() {
      loop()
      setInterval(loop, config.get('check_interval')*1000)
      firstRun = false;
    });
  }
});

client.on('message', message => {
  if (!listen) return;
  if (message.author.bot) return;
  // console.log(message);
  console.log('[message]', message.channel.id, message.author.id, message.content);
  if (message.content.toLowerCase().startsWith(config.get('prefix'))) {
    var args = message.content.slice(config.get('prefix').length).split(/ +/);
    var commandName = args.shift().toLowerCase();
    var command = client.commands.get(commandName) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName))
    if (!command) return;
    try {
      command.execute(message, args);
    }
    catch (error) {
      console.error(error);
      message.reply('there was an error trying to execute that command!');
    }
  }
});

function getLatestCounts() {
  return new Promise(function(resolve, reject) {
    db.ref('counts').on('value', function(snaps) {
      snaps.forEach(function(snap) {
        counts[snap.key] = snap.val();
      });
      console.log(counts);
      resolve();
    });
  });
}

function notifyAnnouncement(ann) {
  console.log('NOTIFY!', ann.name);
  var message = `\`NEW ANNOUNCEMENT #${ann.id}\` [${ann.createdDate}] @everyone  \n**${ann.name}**  \n${ann.desc}`
  if (ann.files.length) {
    message += '\n  __FILES__  \n'
    for (file of ann.files) {
      message += `  \n* http://www.pesuacademy.com/MAcademy/previewAnnouncementFile/${file}`
    }
  }
  for (channelid of config.get('discord_channelids')) {
    client.channels.get(channelid).send(message, {split: true});
  }
  return;
}

async function getAnnouncements() {
  if (config.get('fakeFetch')) {
    return axios.get('https://api.jsonbin.io/b/5b95322bd6fe677c48d7969e/latest', {headers: {'secret-key': config.get('jsonbin_token')}}).then(res => {
      return res.data;
    }).catch(err => { console.error(err); });
  } else {
    return axios.post('http://www.pesuacademy.com/MAcademy/mobile/dispatcher', querystring.stringify({action: '20', mode: '1', minLimit: '0', limit: '10'})).then(res => {
      return res.data;
    }).catch(err => { console.error(err); });
  }
}

async function loop() {
  console.log(new Date() + ' Looping');
  var announcements = await getAnnouncements();
  console.log(announcements.length, 'announcements');
  for (var i = announcements.length-1; i >= 0; i--) {
    console.log(announcements[i].id, ' - ' + announcements[i].name);
    if (announcements[i].id > counts.latestAnnouncementId) {
      db.ref('counts/latestAnnouncementId').set(announcements[i].id);
      db.ref('announcements/'+announcements[i].id).set(announcements[i]);
      notifyAnnouncement(announcements[i]);
    }
  }
}

client.login(config.get('discord_token'));


// One time runs

async function updateCalendar() {
  var c = 0;
  var res = await axios.post('http://www.pesuacademy.com/MAcademy/mobile/dispatcher', querystring.stringify({action: '14', mode: '1', minLimit: '0', limit: '10'}));
  // console.log(res.data);
  console.log('Got calendar data');
  for (evnt of res.data) {
    db.ref('calendar').push().set(evnt).then(() => {
      console.log('set', ++c);
    });
  }
}
// updateCalendar();
