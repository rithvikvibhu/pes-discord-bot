const config = require('config');
const moment = require('moment');

const monthFormats = ['MMMM', 'MMM', 'MM', 'M', 'MMMM YYYY', 'MMM YYYY', 'MM YYYY', 'M YYYY'];

module.exports = [
  {
    name: 'help',
    description: 'List all of my commands or info about a specific command.',
    aliases: ['commands'],
    usage: '[command name]',
    execute(message, args) {
      const data = [];
      const { commands } = message.client;

      if (!args.length) {
        data.push('Here\'s a list of all my commands:');
        data.push(commands.map(command => command.name).join(', '));
        data.push(`\nYou can send \`${config.get('prefix')}help [command name]\` to get info on a specific command!`);
        return message.reply(data, { split: true });
      } else {
        const name = args[0].toLowerCase();
        const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

        if (!command) { return message.reply('that\'s not a valid command!') }

        data.push(`**Name:** ${command.name}`);
        if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
        if (command.description) data.push(`**Description:** ${command.description}`);
        if (command.usage) data.push(`**Usage:** ${config.get('prefix')}${command.name} ${command.usage}`);
        if (command.example) data.push(`**Example:** ${config.get('prefix')}${command.name} ${command.example}`);

        message.channel.send(data, { split: true });
      }
    }
  },
  {
    name: 'ping',
    description: 'Ping!',
    execute(message, args) {
      message.channel.send('Pong.');
    }
  },
  {
    name: 'holiday',
    aliases: [ 'holidays' ],
    description: 'List all holidays in a given month',
    usage: '[month]',
    example: 'Nov',
    execute(message, args) {
      var data = [];
      if (args.length) {
        if (!moment(args.join(' '), monthFormats).isValid()) return message.channel.send('Invalid month.');
        var start = moment(args.join(' '), monthFormats).startOf('month');
        var end = moment(args.join(' '), monthFormats).endOf('month');
      } else {
        var start = moment().startOf('month');
        var end = moment().endOf('month');
      }
      data.push(`\`HOLIDAYS\` (${start.format('Do MMM')} - ${end.format('Do MMM YYYY')})\n`);
      for (evnt of calData) {
        if (evnt.isHoliday == 1 || evnt.isClass == 0) {
          if (moment(evnt.startDate).isBetween(start, end, null, '[)') || moment(evnt.endDate).isBetween(start, end)) {
            data.push(`* ${moment(evnt.startDate).utcOffset(+330).format('Do MMM')} - ${evnt.description}`)
          }
        }
      }
      if (data.length == 1) { data.push('None :(') }
      message.channel.send(data, {split: true});
    }
  },
  {
    name: 'announcements',
    description: 'Get 5 latest announcements\' titles. If an announcement id is mentioned, display more info about that announcement.',
    aliases: [ 'ann', 'anns' ],
    usage: '[announcement id]',
    example: '116',
    execute(message, args) {
      var data = [];
      if (args.length) {
        db.ref('announcements/'+args[0]).once('value', snap => {
          var ann = snap.val();
          data.push(`\`ANNOUNCEMENT #${ann.id}\` [${ann.createdDate}]  \n**${ann.name}**  \n${ann.desc}`);
          if (ann.files.length) {
            data.push('__FILES__');
            for (file of ann.files) {
              data.push(`* http://www.pesuacademy.com/MAcademy/previewAnnouncementFile/${file}`);
            }
          }
          message.channel.send(data, {split: true});
        });
      } else {
        db.ref('announcements').orderByKey().limitToLast(5).once('value', snaps => {
          snaps.forEach(snap => {
            var ann = snap.val();
            data.push(`\`ANNOUNCEMENT #${ann.id}\` [${ann.createdDate}] - ${ann.name}`)
          });
          data.push(`For more info about an announcement, send \`${config.get('prefix')}announcement <id>\`.`);
          message.channel.send(data, {split: true});
        });
      }
    }
  }
];
